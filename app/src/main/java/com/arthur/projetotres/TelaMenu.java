package com.arthur.projetotres;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TelaMenu extends AppCompatActivity implements View.OnClickListener {

    private Button btnretorna;
    private Button btnnp,btnmp;
    private Button btninicio,btnsobre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_menu);
        btnretorna = (Button)findViewById(R.id.btnretorna);
        btnretorna.setOnClickListener(this);
        btnnp = (Button)findViewById(R.id.btnnp);
        btnnp.setOnClickListener(this);
        btnmp = (Button)findViewById(R.id.btnmp);
        btnmp.setOnClickListener(this);
        btninicio = (Button)findViewById(R.id.btninicio);
        btninicio.setOnClickListener(this);
        btnsobre = (Button)findViewById(R.id.btnSobre);
        btnsobre.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == btninicio ){
            Intent a = new Intent (this,TelaInicial.class);
            startActivity(a);
        }
        if(view == btnnp ){
            Intent a = new Intent (this,PaletaUm.class);
            startActivity(a);
        }
        if(view == btnmp ){
            Intent a = new Intent (this,TelaMinhasPaletas.class);
            startActivity(a);
        }
        if(view == btnretorna ){
            finish();
        }
        if(view == btnsobre ){
            Intent a = new Intent (this,TelaSobre.class);
            startActivity(a);
        }
    }
}
