package com.arthur.projetotres;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class TelaInicial extends AppCompatActivity implements View.OnClickListener {
    private Button btn2,btn3,btn1;
    private ImageButton btnMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicial);
        btn2 = (Button)findViewById(R.id.btncriar);
        btn2.setOnClickListener(this);
        btn3 = (Button)findViewById(R.id.btnmp);
        btn3.setOnClickListener(this);
        btnMenu = (ImageButton)findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
//        if(view == btn1 ){
//            Intent i = new Intent (this,TelaMenu.class);
//            Bundle b = new Bundle();
//            i.putExtras(b);
//            startActivity(i);
//        }
        if (view == btn2){
            Intent a= new Intent (this,PaletaUm.class);
            Bundle c= new Bundle ();
            a.putExtras(c);
            startActivity(a);
        }
        if(view == btnMenu){
            Intent id = new Intent(this,TelaMenu.class);
            startActivity(id);
        }
    }
}