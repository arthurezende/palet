package com.arthur.projetotres;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TelaMinhasPaletas extends AppCompatActivity {

    private Button c1,c2,c3,c4,c5;
    private TextView nomepaleta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_minhas_paletas);
        c1 = (Button)findViewById(R.id.b1);
        c2 = (Button)findViewById(R.id.b2);
        c3 = (Button)findViewById(R.id.b3);
        c4 = (Button)findViewById(R.id.b4);
        c5 = (Button)findViewById(R.id.b5);
        nomepaleta = (TextView)findViewById(R.id.paletanome);
        SharedPreferences pref = getSharedPreferences("1", MODE_PRIVATE);
        String nome = pref.getString("nome",null);
        int cor1 = pref.getInt("cor1",0);
        int cor2 = pref.getInt("cor2",0);
        int cor3 = pref.getInt("cor3",0);
        int cor4 = pref.getInt("cor4",0);
        int cor5 = pref.getInt("cor5",0);
        c1.setBackgroundColor(cor1);
        c2.setBackgroundColor(cor2);
        c3.setBackgroundColor(cor3);
        c4.setBackgroundColor(cor4);
        c5.setBackgroundColor(cor5);
        nomepaleta.setText(nome);

    }
}
