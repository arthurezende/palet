package com.arthur.projetotres;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class PaletaDois extends AppCompatActivity implements View.OnClickListener{

    private String rcaracprinc;
    private int rhminprinc;
    private int rhmaxprinc;
    private int rsprinc;
    private int rlprinc;
    private int ridprinc;
    private Button btnVoltar, btnMenu, btn1, btn2, btn3, btn4, btn5, btn6, btnSortear;
    private ArrayList<Apoio> lista;
    private TextView titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paleta_dois);
        Intent i = getIntent();
        if (i != null) {
            Bundle b = new Bundle();
            b = i.getExtras();
            if (b != null) {
                //recebendo do bundle que veio da outra tela
                rcaracprinc = b.getString("caract");
                rhminprinc = b.getInt("hmin");
                rhmaxprinc = b.getInt("hmax");
                rsprinc = b.getInt("s");
                rlprinc = b.getInt("l");
                ridprinc = b.getInt("id");
                //verificando se os dados chegaram
                Log.d("##", rcaracprinc);
                Log.d(getClass().getName(), "rhmin = " + rhminprinc);
                Log.d(getClass().getName(), "rhmax = " + rhmaxprinc);
                Log.d(getClass().getName(), "rsprinc = " + rsprinc);
                Log.d(getClass().getName(), "rlprinc = " + rlprinc);
                Log.d(getClass().getName(), "ridprinc = " + ridprinc);
            }
        }
        btnMenu = (Button)findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(this);
        btn1 = (Button)findViewById(R.id.btn1);
        btn1.setOnClickListener(this);
        btn2 = (Button)findViewById(R.id.btn2);
        btn2.setOnClickListener(this);
        btn3 = (Button)findViewById(R.id.btn3);
        btn3.setOnClickListener(this);
        btn4 = (Button)findViewById(R.id.btn4);
        btn4.setOnClickListener(this);
        btn5 = (Button)findViewById(R.id.btn5);
        btn5.setOnClickListener(this);
        btn6 = (Button)findViewById(R.id.btn6);
        btn6.setOnClickListener(this);
        btnSortear = (Button)findViewById(R.id.btnSortear);
        btnSortear.setOnClickListener(this);
        btnVoltar = (Button)findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(this);
        titulo = (TextView)findViewById(R.id.textView);
        titulo.setText("Escolha uma característica secundária");
        carregarElementos();
        iniciar();
    }

    public void sortear(){
        Collections.shuffle(lista); /* para embaralhar a lista */
    }
    public void iniciar() {
        sortear();
        Random r = new Random();
        /*int posicao = r.nextInt(5); gerar inteiro até 3  */
        /*if(posicao == 0){*/
        btn1.setText(lista.get(0).getCaracteristica());
        btn2.setText(lista.get(1).getCaracteristica());
        btn3.setText(lista.get(2).getCaracteristica());
        btn4.setText(lista.get(3).getCaracteristica());
        btn5.setText(lista.get(4).getCaracteristica());
        btn6.setText(lista.get(5).getCaracteristica());
        btn1.setBackgroundColor(Color.WHITE);
        btn2.setBackgroundColor(Color.WHITE);
        btn3.setBackgroundColor(Color.WHITE);
        btn4.setBackgroundColor(Color.WHITE);
        btn5.setBackgroundColor(Color.WHITE);
        btn6.setBackgroundColor(Color.WHITE);
    }

    public void carregarElementos(){

        lista = new ArrayList<Apoio>();
        Apoio a = new Apoio();
        a.setId(1);
        a.setCaracteristica("Paixão");
        a.setHmax(337);
        a.setHmin(360);
        a.setS(100);
        a.setL(40);
        lista.add(a);

        Apoio b = new Apoio();
        a.setId(2);
        b.setCaracteristica("Paixão");
        b.setHmax(273);
        b.setHmin(288);
        b.setS(100);
        b.setL(40);
        lista.add(b);

        Apoio c = new Apoio();
        a.setId(3);
        c.setCaracteristica("Saúde");
        c.setHmax(0);
        c.setHmin(0);
        c.setS(100);
        c.setL(40);
        lista.add(c);

        Apoio d = new Apoio();
        a.setId(4);
        d.setCaracteristica("Saúde");
        d.setHmin(10);
        d.setHmax(13);
        d.setS(30);
        d.setL(100);
        lista.add(d);

        Apoio e = new Apoio();
        a.setId(5);
        e.setCaracteristica("Saúde");
        e.setHmin(166);
        e.setHmax(188);
        e.setS(30);
        e.setL(100);
        lista.add(e);

        Apoio f = new Apoio();
        a.setId(6);
        f.setCaracteristica("Amor");
        f.setHmin(349);
        f.setHmax(360);
        f.setS(100);
        f.setL(100);
        lista.add(f);

        Apoio k = new Apoio();
        a.setId(11);
        k.setCaracteristica("Amor");
        k.setHmin(349);
        k.setHmax(360);
        k.setS(100);
        k.setL(92);
        lista.add(k);

        Apoio g = new Apoio();
        a.setId(7);
        g.setCaracteristica("Felicidade");
        g.setHmin(45);
        g.setHmax(58);
        g.setS(100);
        g.setL(100);
        lista.add(g);

        Apoio h = new Apoio();
        a.setId(8);
        h.setCaracteristica("Acolhimento");
        h.setHmin(261);
        h.setHmax(296);
        h.setS(34);
        h.setL(90);
        lista.add(h);

        Apoio i = new Apoio();
        a.setId(9);
        i.setCaracteristica("Caridade");
        i.setHmin(166);
        i.setHmax(199);
        i.setS(86);
        i.setL(100);
        lista.add(i);

        Apoio j = new Apoio();
        a.setId(10);
        j.setCaracteristica("Inteligência");
        j.setHmin(210);
        j.setHmax(225);
        j.setS(100);
        j.setL(100);
        lista.add(j);

        Apoio l = new Apoio();
        a.setId(13);
        l.setCaracteristica("Agilidade");
        l.setHmin(349);
        l.setHmax(360);
        l.setS(98);
        l.setL(100);
        lista.add(l);

        Apoio m = new Apoio();
        a.setId(14);
        m.setCaracteristica("Compaixão");
        m.setHmin(349);
        m.setHmax(360);
        m.setS(64);
        m.setL(100);
        lista.add(m);

        Apoio n = new Apoio();
        a.setId(15);
        n.setCaracteristica("Animal");
        n.setHmin(349);
        n.setHmax(360);
        n.setS(68);
        n.setL(77);
        lista.add(n);

        Apoio o = new Apoio();
        a.setId(16);
        o.setCaracteristica("Sabedoria");
        o.setHmin(349);
        o.setHmax(360);
        o.setS(64);
        o.setL(100);
        lista.add(o);

        Apoio p = new Apoio();
        a.setId(17);
        p.setCaracteristica("Família");
        p.setHmin(349);
        p.setHmax(360);
        p.setS(48);
        p.setL(88);
        lista.add(p);

        Apoio q = new Apoio();
        a.setId(18);
        q.setCaracteristica("Família");
        q.setHmin(349);
        q.setHmax(360);
        q.setS(66);
        q.setL(100);
        lista.add(q);

        Apoio r = new Apoio();
        a.setId(19);
        r.setCaracteristica("Juventude");
        r.setHmin(349);
        r.setHmax(360);
        r.setS(100);
        r.setL(70);
        lista.add(r);

        Apoio s = new Apoio();
        a.setId(20);
        s.setCaracteristica("Juventude");
        s.setHmin(349);
        s.setHmax(360);
        s.setS(100);
        s.setL(100);
        lista.add(s);

        Apoio t = new Apoio();
        a.setId(21);
        t.setCaracteristica("Movimento");
        t.setHmin(349);
        t.setHmax(360);
        t.setS(100);
        t.setL(100);
        lista.add(t);

        Apoio u = new Apoio();
        a.setId(22);
        u.setCaracteristica("Movimento");
        u.setHmin(349);
        u.setHmax(360);
        u.setS(100);
        u.setL(100);
        lista.add(u);

        Apoio v = new Apoio();
        a.setId(23);
        v.setCaracteristica("Fome");
        v.setHmin(349);
        v.setHmax(360);
        v.setS(100);
        v.setL(100);
        lista.add(v);

        Apoio w = new Apoio();
        a.setId(24);
        w.setCaracteristica("Infância");
        w.setHmin(349);
        w.setHmax(360);
        w.setS(100);
        w.setL(100);
        lista.add(w);

        Apoio x = new Apoio();
        a.setId(25);
        x.setCaracteristica("Saciedade");
        x.setHmin(349);
        x.setHmax(360);
        x.setS(30);
        x.setL(100);
        lista.add(x);

        Apoio y = new Apoio();
        a.setId(26);
        y.setCaracteristica("Otimismo");
        y.setHmin(349);
        y.setHmax(360);
        y.setS(84);
        y.setL(100);
        lista.add(y);

        Apoio z = new Apoio();
        a.setId(27);
        z.setCaracteristica("Calor");
        z.setHmax(337);
        z.setHmin(360);
        z.setS(100);
        z.setL(100);
        lista.add(z);

        Apoio aa = new Apoio();
        a.setId(28);
        aa.setCaracteristica("Frio");
        aa.setHmax(273);
        aa.setHmin(288);
        aa.setS(15);
        aa.setL(100);
        lista.add(aa);

        Apoio ab = new Apoio();
        a.setId(29);
        ab.setCaracteristica("Inovação");
        ab.setHmax(0);
        ab.setHmin(0);
        ab.setS(92);
        ab.setL(100);
        lista.add(ab);

        Apoio ac = new Apoio();
        a.setId(30);
        ac.setCaracteristica("Aprendizagem");
        ac.setHmin(10);
        ac.setHmax(13);
        ac.setS(60);
        ac.setL(60);
        lista.add(ac);

        Apoio ad = new Apoio();
        a.setId(31);
        ad.setCaracteristica("Fantasia");
        ad.setHmin(166);
        ad.setHmax(188);
        ad.setS(90);
        ad.setL(90);
        lista.add(ad);

        Apoio ae = new Apoio();
        a.setId(32);
        ae.setCaracteristica("Inovação");
        ae.setHmin(349);
        ae.setHmax(360);
        ae.setS(76);
        ae.setL(100);
        lista.add(ae);

        Apoio af = new Apoio();
        a.setId(33);
        af.setCaracteristica("Confiança");
        af.setHmin(349);
        af.setHmax(360);
        af.setS(100);
        af.setL(0);
        lista.add(af);

        Apoio ag = new Apoio();
        a.setId(34);
        ag.setCaracteristica("Responsabilidade");
        ag.setHmin(45);
        ag.setHmax(58);
        ag.setS(73);
        ag.setL(24);
        lista.add(ag);

        Apoio ah = new Apoio();
        a.setId(35);
        ah.setCaracteristica("Liderança");
        ah.setHmin(261);
        ah.setHmax(296);
        ah.setS(0);
        ah.setL(100);
        lista.add(ah);

        Apoio ai = new Apoio();
        a.setId(36);
        ai.setCaracteristica("Liderança");
        ai.setHmin(166);
        ai.setHmax(199);
        ai.setS(100);
        ai.setL(78);
        lista.add(ai);

        Apoio aj = new Apoio();
        a.setId(37);
        aj.setCaracteristica("Poder");
        aj.setHmin(210);
        aj.setHmax(225);
        aj.setS(100);
        aj.setL(20);
        lista.add(aj);

        Apoio ak = new Apoio();
        a.setId(38);
        ak.setCaracteristica("Natureza");
        ak.setHmin(349);
        ak.setHmax(360);
        ak.setS(100);
        ak.setL(100);
        lista.add(ak);

        Apoio al = new Apoio();
        a.setId(39);
        al.setCaracteristica("Natureza");
        al.setHmin(349);
        al.setHmax(360);
        al.setS(100);
        al.setL(44);
        lista.add(al);

        Apoio am = new Apoio();
        a.setId(40);
        am.setCaracteristica("Riqueza");
        am.setHmin(349);
        am.setHmax(360);
        am.setS(100);
        am.setL(100);
        lista.add(am);

        Apoio an = new Apoio();
        a.setId(41);
        an.setCaracteristica("Atencioso");
        an.setHmin(349);
        an.setHmax(360);
        an.setS(71);
        an.setL(84);
        lista.add(an);

        Apoio ao = new Apoio();
        a.setId(42);
        ao.setCaracteristica("Afetuoso");
        ao.setHmin(349);
        ao.setHmax(360);
        ao.setS(29);
        ao.setL(16);
        lista.add(ao);

        Apoio ap = new Apoio();
        a.setId(43);
        ap.setCaracteristica("Aventureiro");
        ap.setHmin(349);
        ap.setHmax(360);
        ap.setS(89);
        ap.setL(61);
        lista.add(ap);

        Apoio aq = new Apoio();
        a.setId(44);
        aq.setCaracteristica("Benevolente");
        aq.setHmin(349);
        aq.setHmax(360);
        aq.setS(60);
        aq.setL(100);
        lista.add(aq);

        Apoio ar = new Apoio();
        a.setId(45);
        ar.setCaracteristica("Brasileiro");
        ar.setHmin(349);
        ar.setHmax(360);
        ar.setS(73);
        ar.setL(41);
        lista.add(ar);

        Apoio as = new Apoio();
        a.setId(46);
        as.setCaracteristica("Criatividade");
        as.setHmin(349);
        as.setHmax(360);
        as.setS(100);
        as.setL(100);
        lista.add(as);

        Apoio at = new Apoio();
        a.setId(47);
        at.setCaracteristica("Carisma");
        at.setHmin(349);
        at.setHmax(360);
        at.setS(26);
        at.setL(100);
        lista.add(at);

        Apoio au = new Apoio();
        a.setId(48);
        au.setCaracteristica("Cheiroso");
        au.setHmin(349);
        au.setHmax(360);
        au.setS(6);
        au.setL(100);
        lista.add(au);

        Apoio av = new Apoio();
        a.setId(49);
        av.setCaracteristica("Cinematográfico");
        av.setHmin(349);
        av.setHmax(360);
        av.setS(82);
        av.setL(60);
        lista.add(av);

        Apoio aw = new Apoio();
        a.setId(50);
        aw.setCaracteristica("Tropical");
        aw.setHmin(349);
        aw.setHmax(360);
        aw.setS(100);
        aw.setL(100);
        lista.add(aw);

        Apoio ax = new Apoio();
        a.setId(51);
        ax.setCaracteristica("Calculista");
        ax.setHmin(349);
        ax.setHmax(360);
        ax.setS(51);
        ax.setL(47);
        lista.add(ax);

        Apoio ay = new Apoio();
        a.setId(52);
        ay.setCaracteristica("Ágil");
        ay.setHmin(349);
        ay.setHmax(360);
        ay.setS(100);
        ay.setL(0);
        lista.add(ay);

        Apoio az = new Apoio();
        a.setId(53);
        az.setCaracteristica("Eficaz");
        az.setHmin(349);
        az.setHmax(360);
        az.setS(100);
        az.setL(100);
        lista.add(az);

        Apoio ba = new Apoio();
        ba.setId(54);
        ba.setCaracteristica("Seriedade");
        ba.setHmin(349);
        ba.setHmax(360);
        ba.setS(100);
        ba.setL(0);
        lista.add(ba);

        Apoio bb = new Apoio();
        bb.setId(55);
        bb.setCaracteristica("Animada");
        bb.setHmin(349);
        bb.setHmax(360);
        bb.setS(95);
        bb.setL(85);
        lista.add(bb);

//        removerRepetidos();

    }
//
//    public void removerRepetidos(){
//        int
//        lista.remove(ridprinc);
//    }

    @Override
    public void onClick(View view) {
        if(view == btnVoltar){
            finish();
        }
        if(view == btn1){
            String caract = new String();
            int hmin = 0;
            int hmax = 0;
            int s = 0;
            int l = 0;
            int id = 0;
            caract = lista.get(0).getCaracteristica();
            hmin = lista.get(0).getHmin();
            hmax = lista.get(0).getHmax();
            s = lista.get(0).getS();
            l = lista.get(0).getL();
            id = lista.get(0).getId();
            Intent i = new Intent(this,PaletaTres.class);
                Bundle b = new Bundle();
                    //adiciona a caracteristica da tela anterior no novo bundle
                    b.putString("rc",rcaracprinc);
                    b.putInt("hminc",rhminprinc);
                    b.putInt("hmaxc",rhmaxprinc);
                    b.putInt("lc",rlprinc);
                    b.putInt("sc",rsprinc);
                    b.putInt("idc",ridprinc);
                    //adiciona a caracteristica dessa tela no bundle
                    b.putString("rs",caract);
                    b.putInt("hmins",hmin);
                    b.putInt("hmaxs",hmax);
                    b.putInt("ss",s);
                    b.putInt("ls",l);
                    b.putInt("ids",id);
                    i.putExtras(b);
                    startActivity(i);
            }
        if(view==btn2){
            String caract = new String();
            int hmin = 0;
            int hmax = 0;
            int s = 0;
            int l = 0;
            int id = 0;
            caract = lista.get(1).getCaracteristica();
            hmin = lista.get(1).getHmin();
            hmax = lista.get(1).getHmax();
            s = lista.get(1).getS();
            l = lista.get(1).getL();
            id = lista.get(1).getId();
            Intent i = new Intent(this,PaletaTres.class);
            Bundle b = new Bundle();
            //adiciona a caracteristica da tela anterior no novo bundle
            b.putString("rc",rcaracprinc);
            b.putInt("hminc",rhminprinc);
            b.putInt("hmaxc",rhmaxprinc);
            b.putInt("lc",rlprinc);
            b.putInt("sc",rsprinc);
            b.putInt("idc",ridprinc);
            //adiciona a caracteristica dessa tela no bundle
            b.putString("rs",caract);
            b.putInt("hmins",hmin);
            b.putInt("hmaxs",hmax);
            b.putInt("ss",s);
            b.putInt("ls",l);
            b.putInt("ids",id);
            i.putExtras(b);
            startActivity(i);
        }
        if(view==btn3){
            String caract = new String();
            int hmin = 0;
            int hmax = 0;
            int s = 0;
            int l = 0;
            int id = 0;
            caract = lista.get(2).getCaracteristica();
            hmin = lista.get(2).getHmin();
            hmax = lista.get(2).getHmax();
            s = lista.get(2).getS();
            l = lista.get(2).getL();
            id = lista.get(2).getId();
            Intent i = new Intent(this,PaletaTres.class);
            Bundle b = new Bundle();
            //adiciona a caracteristica da tela anterior no novo bundle
            b.putString("rc",rcaracprinc);
            b.putInt("hminc",rhminprinc);
            b.putInt("hmaxc",rhmaxprinc);
            b.putInt("lc",rlprinc);
            b.putInt("sc",rsprinc);
            b.putInt("idc",ridprinc);
            //adiciona a caracteristica dessa tela no bundle
            b.putString("rs",caract);
            b.putInt("hmins",hmin);
            b.putInt("hmaxs",hmax);
            b.putInt("ss",s);
            b.putInt("ls",l);
            b.putInt("ids",id);
            i.putExtras(b);
            startActivity(i);
        }

        if(view==btn4){
            String caract = new String();
            int hmin = 0;
            int hmax = 0;
            int s = 0;
            int l = 0;
            int id = 0;
            caract = lista.get(4).getCaracteristica();
            hmin = lista.get(4).getHmin();
            hmax = lista.get(4).getHmax();
            s = lista.get(4).getS();
            l = lista.get(4).getL();
            id = lista.get(4).getId();
            Intent i = new Intent(this,PaletaTres.class);
            Bundle b = new Bundle();
            //adiciona a caracteristica da tela anterior no novo bundle
            b.putString("rc",rcaracprinc);
            b.putInt("hminc",rhminprinc);
            b.putInt("hmaxc",rhmaxprinc);
            b.putInt("lc",rlprinc);
            b.putInt("sc",rsprinc);
            b.putInt("idc",ridprinc);
            //adiciona a caracteristica dessa tela no bundle
            b.putString("rs",caract);
            b.putInt("hmins",hmin);
            b.putInt("hmaxs",hmax);
            b.putInt("ss",s);
            b.putInt("ls",l);
            b.putInt("ids",id);
            i.putExtras(b);
            startActivity(i);
        }
        if(view==btn5){
            String caract = new String();
            int hmin = 0;
            int hmax = 0;
            int s = 0;
            int l = 0;
            int id = 0;
            caract = lista.get(4).getCaracteristica();
            hmin = lista.get(4).getHmin();
            hmax = lista.get(4).getHmax();
            s = lista.get(4).getS();
            l = lista.get(4).getL();
            id = lista.get(4).getId();
            Intent i = new Intent(this,PaletaTres.class);
            Bundle b = new Bundle();
            //adiciona a caracteristica da tela anterior no novo bundle
            b.putString("rc",rcaracprinc);
            b.putInt("hminc",rhminprinc);
            b.putInt("hmaxc",rhmaxprinc);
            b.putInt("lc",rlprinc);
            b.putInt("sc",rsprinc);
            b.putInt("idc",ridprinc);
            //adiciona a caracteristica dessa tela no bundle
            b.putString("rs",caract);
            b.putInt("hmins",hmin);
            b.putInt("hmaxs",hmax);
            b.putInt("ss",s);
            b.putInt("ls",l);
            b.putInt("ids",id);
            i.putExtras(b);
            startActivity(i);
        }

        if(view==btn6){
            String caract = new String();
            int hmin = 0;
            int hmax = 0;
            int s = 0;
            int l = 0;
            int id = 0;
            caract = lista.get(5).getCaracteristica();
            hmin = lista.get(5).getHmin();
            hmax = lista.get(5).getHmax();
            s = lista.get(5).getS();
            l = lista.get(5).getL();
            id = lista.get(5).getId();
            Intent i = new Intent(this,PaletaTres.class);
            Bundle b = new Bundle();
            //adiciona a caracteristica da tela anterior no novo bundle
            b.putString("rc",rcaracprinc);
            b.putInt("hminc",rhminprinc);
            b.putInt("hmaxc",rhmaxprinc);
            b.putInt("lc",rlprinc);
            b.putInt("sc",rsprinc);
            b.putInt("idc",ridprinc);
            //adiciona a caracteristica dessa tela no bundle
            b.putString("rs",caract);
            b.putInt("hmins",hmin);
            b.putInt("hmaxs",hmax);
            b.putInt("ss",s);
            b.putInt("ls",l);
            b.putInt("ids",id);
            i.putExtras(b);
            startActivity(i);
        }

        if(view == btnSortear){
            iniciar();
        }
        if(view == btnVoltar){
            finish();
        }
        if(view == btnMenu){
            Intent id = new Intent(this,TelaMenu.class);
            startActivity(id);
        }
    }
}
