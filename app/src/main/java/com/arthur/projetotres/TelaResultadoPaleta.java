package com.arthur.projetotres;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class TelaResultadoPaleta extends AppCompatActivity implements View.OnClickListener{

    private Button btn1,btn2,btn3,btn4,btn5,btnReiniciar,btnSalvar;
    private ImageButton btnMenu;
    private TextView textCarac,textHex,textHsv;
    private String rc,rs,rt,ru,rv;
    private int hminc,hmins,hmint,hminu,hminv; //H MINIMO
    private int hmaxc,hmaxs,hmaxt,hmaxu,hmaxv; //H MAXIMO
    private int htc,hts,htt,htu,htv;
    private int sc,ss,st,su,sv;
    private int lc,ls,lt,lu,lv;
    private int cor1,cor2,cor3,cor4,cor5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_resultado_paleta);
        Intent i = getIntent();
        textCarac = (TextView) findViewById(R.id.textCarac);
        textHex = (TextView) findViewById(R.id.textHex);
        textHsv = (TextView) findViewById(R.id.textHsv);
        btn1 = (Button)findViewById(R.id.btn1);
        btn1.setOnClickListener(this);
        btn2 = (Button)findViewById(R.id.btn2);
        btn2.setOnClickListener(this);
        btn3 = (Button)findViewById(R.id.btn3);
        btn3.setOnClickListener(this);
        btn4 = (Button)findViewById(R.id.btn4);
        btn4.setOnClickListener(this);
        btn5 = (Button)findViewById(R.id.btn5);
        btn5.setOnClickListener(this);
        btnReiniciar = (Button)findViewById(R.id.btnReiniciar);
        btnReiniciar.setOnClickListener(this);
        btnSalvar = (Button)findViewById(R.id.btnSalvarPaleta);
        btnSalvar.setOnClickListener(this);
        btnMenu = (ImageButton)findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(this);
        Log.d("##", "antes bundle");

        if (i != null) {
            Bundle b = new Bundle();
            b = i.getExtras();
            Log.d("##", "intent ok");

            if (b != null) {
                //caracteristica 1
                rc = b.getString("rc");
                hminc = b.getInt("hminc");
                hmaxc = b.getInt("hmaxc");
                sc = b.getInt("sc");
                lc = b.getInt("lc");

                //caracteristica 2
                rs = b.getString("rs");
                hmins = b.getInt("hmins");
                hmaxs= b.getInt("hmaxs");
                ss = b.getInt("ss");
                ls = b.getInt("ls");

                //caracteristica 3
                rt = b.getString("rt");
                hmint = b.getInt("hmint");
                hmaxt = b.getInt("hmaxt");
                st = b.getInt("st");
                lt = b.getInt("lt");

                //caracteristica 4
                ru = b.getString("ru");
                hminu = b.getInt("hminu");
                hmaxu = b.getInt("hmaxu");
                su = b.getInt("su");
                lu = b.getInt("lu");

                //caracteristica 5
                rv = b.getString("rv");
                hminv = b.getInt("hminv");
                hmaxv = b.getInt("hmaxv");
                sv = b.getInt("sv");
                lv = b.getInt("lv");

                Log.d("##", rc);
                Log.d(getClass().getName(), "hminc = " + hminc);
                Log.d(getClass().getName(), "hmaxc = " + hmaxc);
                Log.d(getClass().getName(), "sc = " + sc);
                Log.d(getClass().getName(), "lc = " + lc);

            }
        }

        if(hminc<hmaxc){
            htc = aleatoriar(hminc,hmaxc);
            Log.d(getClass().getName(), "primeiro if htc = " + htc);
        }else{
            Random r = new Random();
            int qual = r.nextInt(2);
            if(qual==0){
                htc = hminc;
                Log.d(getClass().getName(), "q=0 htc = " + htc);
            }
            else if(qual==1){
                htc = hmaxc;
                Log.d(getClass().getName(), "q=1 htc = " + htc);
            }
        }
        if(hmins<hmaxs){
            hts = aleatoriar(hmins,hmaxs);
            Log.d(getClass().getName(), "primeiro if hts = " + hts);
        }else{
            Random r = new Random();
            int qual = r.nextInt(2);
            if(qual==0){
                hts = hmins;
                Log.d(getClass().getName(), "q=0 hts = " + hts);
            }
            else if(qual==1){
                hts = hmaxs;
                Log.d(getClass().getName(), "q=1 hts = " + hts);
            }
        }
        if(hmint<hmaxt){
            htt = aleatoriar(hmint,hmaxt);
            Log.d(getClass().getName(), "primeiro if htt = " + htt);
        }else{
            Random r = new Random();
            int qual = r.nextInt(2);
            if(qual==0){
                htt = hmint;
                Log.d(getClass().getName(), "q=0 htt = " + htt);
            }
            else if(qual==1){
                htt = hmaxt;
                Log.d(getClass().getName(), "q=1 htt = " + htt);
            }
        }
        if(hminu<hmaxu){
            htu = aleatoriar(hminu,hmaxu);
            Log.d(getClass().getName(), "primeiro if htu = " + htu);
        }else{
            Random r = new Random();
            int qual = r.nextInt(2);
            if(qual==0){
                htu = hminu;
                Log.d(getClass().getName(), "q=0 htu = " + htu);
            }
            else if(qual==1){
                htu = hmaxu;
                Log.d(getClass().getName(), "q=1 htu = " + htu);
            }
        }
        if(hminv<hmaxv){
            htv = aleatoriar(hminv,hmaxv);
            Log.d(getClass().getName(), "primeiro if htv = " + htv);
        }else{
            Random r = new Random();
            int qual = r.nextInt(2);
            if(qual==0){
                htv = hminv;
                Log.d(getClass().getName(), "q=0 htv = " + htv);
            }
            else if(qual==1){
                htv = hmaxv;
                Log.d(getClass().getName(), "q=1 htv = " + htv);
            }
        }
        cor1 = Color.parseColor((TelaResultadoPaleta.hsvToRgb(htc,sc,lc)));
        cor2 = Color.parseColor((TelaResultadoPaleta.hsvToRgb(hts,ss,ls)));
        cor3 = Color.parseColor((TelaResultadoPaleta.hsvToRgb(htt,st,lt)));
        cor4 = Color.parseColor((TelaResultadoPaleta.hsvToRgb(htu,su,lu)));
        cor5 = Color.parseColor((TelaResultadoPaleta.hsvToRgb(htv,sv,lv)));
        btn1.setBackgroundColor(cor1);
        btn2.setBackgroundColor(cor2);
        btn3.setBackgroundColor(cor3);
        btn4.setBackgroundColor(cor4);
        btn5.setBackgroundColor(cor5);

    }

    public void SalvarShared(String paletanome){
        SharedPreferences.Editor editor = getSharedPreferences("1", MODE_PRIVATE).edit();
        editor.putString("rc",rc);
        editor.putInt("htc", htc);
        editor.putInt("sc", sc);
        editor.putInt("lc", lc);

        editor.putString("rs",rs);
        editor.putInt("hts", hts);
        editor.putInt("ss", ss);
        editor.putInt("ls", ls);

        editor.putString("rt",rt);
        editor.putInt("htt", htt);
        editor.putInt("st", st);
        editor.putInt("lt", lt);

        editor.putString("ru",ru);
        editor.putInt("htu", htu);
        editor.putInt("su", su);
        editor.putInt("lu", lu);

        editor.putString("rv",rv);
        editor.putInt("htv", htv);
        editor.putInt("sv", sv);
        editor.putInt("lv", lv);

        editor.putInt("cor1",cor1);
        editor.putInt("cor2",cor2);
        editor.putInt("cor3",cor3);
        editor.putInt("cor4",cor4);
        editor.putInt("cor5",cor5);
        editor.putString("nome",paletanome);

        editor.commit();
        Log.d(getClass().getName(), "salvou essa caceta "  + paletanome);

    }
    public int aleatoriar(int minimo, int maximo) {
        Random random = new Random();
        return random.nextInt((maximo - minimo) + 1) + minimo;
    }
    /**
     * @param H
     *            0-360
     * @param S
     *            0-100
     * @param V
     *            0-100
     * @return color in hex string
     */
    // função para converter HSV para RGB (e exibir acima)
    @NonNull
    public static String hsvToRgb(float H, float S, float V) {

        float R, G, B;

        H /= 360f;
        S /= 100f;
        V /= 100f;

        if (S == 0)
        {
            R = V * 255;
            G = V * 255;
            B = V * 255;
        } else {
            float var_h = H * 6;
            if (var_h == 6)
                var_h = 0; // H must be < 1
            int var_i = (int) Math.floor((double) var_h); // Or ... var_i =
            // floor( var_h )
            float var_1 = V * (1 - S);
            float var_2 = V * (1 - S * (var_h - var_i));
            float var_3 = V * (1 - S * (1 - (var_h - var_i)));

            float var_r;
            float var_g;
            float var_b;
            if (var_i == 0) {
                var_r = V;
                var_g = var_3;
                var_b = var_1;
            } else if (var_i == 1) {
                var_r = var_2;
                var_g = V;
                var_b = var_1;
            } else if (var_i == 2) {
                var_r = var_1;
                var_g = V;
                var_b = var_3;
            } else if (var_i == 3) {
                var_r = var_1;
                var_g = var_2;
                var_b = V;
            } else if (var_i == 4) {
                var_r = var_3;
                var_g = var_1;
                var_b = V;
            } else {
                var_r = V;
                var_g = var_1;
                var_b = var_2;
            }

            R = var_r * 255; // RGB results from 0 to 255
            G = var_g * 255;
            B = var_b * 255;
        }

        String rs = Integer.toHexString((int) (R));
        String gs = Integer.toHexString((int) (G));
        String bs = Integer.toHexString((int) (B));

        if (rs.length() == 1)
            rs = "0" + rs;
        if (gs.length() == 1)
            gs = "0" + gs;
        if (bs.length() == 1)
            bs = "0" + bs;
        return "#" + rs + gs + bs;
    }

//    public String hslparahex(float hue, float saturation, float value) {
//
//        int h = (int)(hue * 6);
//        Log.d(getClass().getName(), "h = " + h);
//        float f = hue * 6 - h;
//        Log.d("d", "f: " + f);
//        float p = value * (1 - saturation);
//        Log.d("d", "p: " + p);
//        float q = value * (1 - f * saturation);
//        Log.d("d", "q: " + q);
//        float t = value * (1 - (1 - f) * saturation);
//        Log.d("d", "t: " + t);
//
//        switch (h) {
//            case 0: return rgbToString(value, t, p);
//            case 1: return rgbToString(q, value, p);
//            case 2: return rgbToString(p, value, t);
//            case 3: return rgbToString(p, q, value);
//            case 4: return rgbToString(t, p, value);
//            case 5: return rgbToString(value, p, q);
//            default: return "erro";
//        }
//    }
//
//    public static String rgbToString(float r, float g, float b) {
//        String rs = Integer.toHexString((int)(r * 256));
//        String gs = Integer.toHexString((int)(g * 256));
//        String bs = Integer.toHexString((int)(b * 256));
//        return rs + gs + bs;
//    }

//    public String hslparahex(int h,int s,int l){
//        int r=0, g=0, b=0, var_2=0, var_1=0,h2=0;
//        String rhex,ghex,bhex,rgbhex;
//        if (s == 0)
//        {
//            r = l * 255;
//            g = l * 255;
//            b = l * 255;
//        }
//        else
//        {
//            if (l < 0.5)
//            {
//                var_2 = l * (1 + s);
//            }
//            else
//            {
//                var_2 = l + s - (s * l);
//            }
//
//            var_1 = 2 * l - var_2;
//            r = 255 * hue_2_rgb(var_1,var_2,h2 + (1 / 3));
//            g = 255 * hue_2_rgb(var_1,var_2,h2);
//            b = 255 * hue_2_rgb(var_1,var_2,h2 - (1 / 3));
//        }
//
//        rhex = String.format("%02X", r);
//        ghex = String.format("%02X", g);
//        bhex = String.format("%02X", b);
//
//        rgbhex = rhex+ghex+bhex;
//        return rgbhex;
//    }
//
//
//    public int hue_2_rgb(int v1,int v2,int vh)
//    {
//        if (vh < 0)
//        {
//            vh += 1;
//        }
//
//        if (vh > 1)
//        {
//            vh -= 1;
//        }
//
//        if ((6 * vh) < 1)
//        {
//            return (v1 + (v2 - v1) * 6 * vh);
//        }
//
//        if ((2 * vh) < 1)
//        {
//            return (v2);
//        }
//
//        if ((3 * vh) < 2)
//        {
//            return (v1 + (v2 - v1) * ((2 / 3 - vh) * 6));
//        }
//
//        return (v1);
//    }

    @Override
    public void onClick(View view) {
        if(view == btn1) {
            textCarac.setText(rc);
            textHex.setText(TelaResultadoPaleta.hsvToRgb(htc,sc,lc));
            textHsv.setText("H:"+htc+" S: "+sc+" V: "+lc);
        }
        if(view == btn2) {
            textCarac.setText(rs);
            textHex.setText(TelaResultadoPaleta.hsvToRgb(htv,ss,ls));
            textHsv.setText("H:"+hts+" S: "+ss+" V: "+ls);
        }
        if(view == btn3) {
            textCarac.setText(rt);
            textHex.setText(TelaResultadoPaleta.hsvToRgb(htt,st,lt));
            textHsv.setText("H:"+htt+" S: "+st+" V: "+lt);
        }
        if(view == btn4) {
            textCarac.setText(ru);
            textHex.setText(TelaResultadoPaleta.hsvToRgb(htu,su,lu));
            textHsv.setText("H:"+htu+" S: "+su+" V: "+lu);
        }
        if(view == btn5) {
            textCarac.setText(rv);
            textHex.setText(TelaResultadoPaleta.hsvToRgb(htv,sv,lv));
            textHsv.setText("H:"+htv+" S: "+sv+" V: "+lv);
        }
        if(view == btnReiniciar){
            Intent id = new Intent(this,TelaInicial.class);
            startActivity(id);
        }
        if(view == btnMenu){
            Intent id = new Intent(this,TelaMenu.class);
            startActivity(id);
        }
        if(view == btnSalvar){
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
            dialogBuilder.setView(dialogView);

            final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

            dialogBuilder.setTitle("Custom dialog");
            dialogBuilder.setMessage("Enter text below");
            dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String paletanome = edt.getText().toString();
                    SalvarShared(paletanome);
                }
            });
            dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //pass
                }
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }
//links salvar na memoria
        //https://developer.android.com/guide/topics/data/data-storage?hl=pt-br
        //https://www.androidpro.com.br/blog/armazenamento-de-dados/armazenar-dados-de-aplicativos-localmente/
        //https://www.klebermota.eti.br/2011/04/25/5-maneiras-de-armazenar-dados-em-aplicacoes-android/
        // links dialog
        //https://developer.android.com/guide/topics/ui/dialogs?hl=pt-br
        //https://www.devmedia.com.br/android-dialog/26749
        //http://blog.alura.com.br/criando-caixas-de-dialogo-no-android-dialogs/
        //https://pt.stackoverflow.com/questions/71207/como-abrir-uma-dialog-no-android
        //https://stackoverflow.com/questions/10903754/input-text-dialog-android
        //https://stackoverflow.com/questions/18799216/how-to-make-a-edittext-box-in-a-dialog
    }
}
